"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const util = require('util');
const fs = require('fs');
const asyncReadFile = util.promisify(fs.readFile);
const app = express();
app.get('/users', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let mockData = yield getUsers("mock-usm.json");
    res.json(mockData);
}));
app.listen('3001', () => {
    console.log('USM is listening on port 3001!');
});
function getUsers(filename) {
    return __awaiter(this, void 0, void 0, function* () {
        let usersFile = yield asyncReadFile(filename, "utf-8");
        return JSON.parse(usersFile);
    });
}
