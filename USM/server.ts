import express = require('express')
const util = require('util');
const fs = require('fs');

const asyncReadFile = util.promisify(fs.readFile);
const app = express()

app.get('/users', async (req, res) => {
    let mockData = await getUsers("mock-usm.json")
    res.json(mockData)
})

app.listen('3001', () => {
    console.log('USM is listening on port 3001!')
})

async function getUsers(filename: string) {
    let usersFile = await asyncReadFile(filename, "utf-8");
    return JSON.parse(usersFile)
}