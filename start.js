"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Parser = exports.RequestTypes = void 0;
const { ReadFile } = require('./readFile');
const util = require('util');
const fs = require('fs');
const asyncReadFile = util.promisify(fs.readFile);
class Variable {
    constructor(index, variableName) {
        this.index = index;
        this.variableName = variableName;
    }
}
class PathVariable {
}
class Operation {
    constructor(bs, endpoint, method, request, order, affect) {
        this.bs = bs;
        this.endpoint = endpoint;
        this.method = method;
        this.request = request;
        this.order = order;
        this.affect = affect;
    }
}
class Request {
}
var RequestTypes;
(function (RequestTypes) {
    RequestTypes["GET"] = "get";
    RequestTypes["POST"] = "post";
    RequestTypes["PUT"] = "put";
    RequestTypes["DELETE"] = "delete";
})(RequestTypes = exports.RequestTypes || (exports.RequestTypes = {}));
class Parser {
    constructor(ruleName) {
        this.ruleName = ruleName;
    }
    generateRequest(requestType, pathVariables) {
        return __awaiter(this, void 0, void 0, function* () {
            let response = "";
            let ruleFile = yield asyncReadFile('rule.cson', 'utf8');
            console.log(ruleFile);
            let data = JSON.parse(ruleFile);
            response = data;
            /*let operations = [Operation]
            data.businessNeeded.forEach((element: typeof Operation) => {
                let operation = new Operation
                operations.push(element);
            });
            operations.at(0)*/
            return response;
        });
    }
}
exports.Parser = Parser;
/*let data = JSON.parse(ruleFile)

let bs = data.businessNeeded

let firstRequest = bs.at(0).request
let secondRequest = bs.at(1).request

let variableData = firstRequest.pathVariables.at(0).value.split(".")

let firstRequestVariables: any[] = []

if (variableData.at(0) === '$') {
    firstRequest.pathVariables.forEach((element: any) => {
        firstRequestVariables.push(element);
    });
}

console.log(firstRequestVariables)
}

/*let grammarFile = fs.readFileSync("json.pegjs", "utf-8");

let parser = peggy.generate(grammarFile);

let ruleFile = fs.readFileSync("rule.cson", "utf-8")

let data = parser.parse(ruleFile);*/
/*let variableJson : string
variableJson = data.pathVariables

let variableData = variableJson.split(".");

if (variableData.at(0) === '$') {
    let variable = new Variable(Number(variableData.at(1)), String(variableData.at(2)))
    console.log(variable)
}*/ 
