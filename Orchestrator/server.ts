import express from 'express';
import app from "../Orchestrator/app";
import { OrchestratorController } from "./controllers/orchestratorController";
import { OrchestratorRoutes } from './routes/orchestratorRoutes';
import * as http from 'http';
import * as fs from 'fs';
const path = require('path')
const PORT = 3000;


http.createServer(app).listen(PORT, () => {
    console.log('Express server listening on port ' + PORT);
    let or: OrchestratorRoutes; 
    or = new OrchestratorRoutes();
    or.loadRules(app);
})

