"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Operation = void 0;
class Operation {
    constructor(bs, endpoint, method, request, order, affect) {
        this.bs = bs;
        this.endpoint = endpoint;
        this.method = method;
        this.request = request;
        this.order = order;
        this.affect = affect;
    }
}
exports.Operation = Operation;
