class UrlMethod {
    url : string;
    mainMethod: string;

    constructor(url: string, mainMethod: string) {
       this.url = url;
       this.mainMethod = mainMethod;
    }
}

export {UrlMethod}