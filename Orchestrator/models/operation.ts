import {OperationRequest} from './request'

class Operation {
    bs: string;
    endpoint: string;
    method: string;
    request: OperationRequest;
    order: number;
    affect: [Operation]

    constructor(bs: string, endpoint: string, method: string, request: OperationRequest, order: number, affect: [Operation]) {
        this.bs = bs;
        this.endpoint = endpoint;
        this.method = method;
        this.request = request;
        this.order = order;
        this.affect = affect;
    }
}

export {Operation}