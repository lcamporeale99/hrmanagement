import { Header } from "./header";
import {Variable} from "./variable"

class OperationRequest {
    pathVariables: [Variable];
    queryParams: [Variable];
    body: JSON;
    header: [Header];

    constructor(pathVariables: [Variable], queryParams: [Variable], body: JSON, header: [Header]) {
        this.pathVariables = pathVariables;
        this.queryParams = queryParams;
        this.body = body;
        this.header = header;
    }
}

export {OperationRequest}