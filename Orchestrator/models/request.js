"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OperationRequest = void 0;
class OperationRequest {
    constructor(pathVariables, queryParams, body, header) {
        this.pathVariables = pathVariables;
        this.queryParams = queryParams;
        this.body = body;
        this.header = header;
    }
}
exports.OperationRequest = OperationRequest;
