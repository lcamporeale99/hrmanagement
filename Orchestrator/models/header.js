"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Header = void 0;
class Header {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}
exports.Header = Header;
