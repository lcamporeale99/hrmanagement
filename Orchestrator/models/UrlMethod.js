"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UrlMethod = void 0;
class UrlMethod {
    constructor(url, mainMethod) {
        this.url = url;
        this.mainMethod = mainMethod;
    }
}
exports.UrlMethod = UrlMethod;
