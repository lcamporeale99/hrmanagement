"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Variable = void 0;
class Variable {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}
exports.Variable = Variable;
