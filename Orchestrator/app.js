"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const orchestratorRoutes_1 = require("./routes/orchestratorRoutes");
class App {
    constructor() {
        this.orchestratorRoutes = new orchestratorRoutes_1.OrchestratorRoutes();
        this.app = (0, express_1.default)();
        this.config();
        //this.orchestratorRoutes.routes(this.app);
    }
    config() {
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: false }));
    }
}
exports.default = new App().app;
