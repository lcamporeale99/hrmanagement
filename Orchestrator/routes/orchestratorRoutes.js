"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrchestratorRoutes = void 0;
const orchestratorController_1 = require("../controllers/orchestratorController");
const urlMethod_1 = require("../models/urlMethod");
const jsonQ = require('jsonq');
const util_1 = __importDefault(require("util"));
const fs_1 = __importDefault(require("fs"));
const asyncReadFile = util_1.default.promisify(fs_1.default.readFile);
const asyncReadDir = util_1.default.promisify(fs_1.default.readdir);
class OrchestratorRoutes {
    constructor() {
        this.orchestratorController = new orchestratorController_1.OrchestratorController();
    }
    loadRules(app) {
        return __awaiter(this, void 0, void 0, function* () {
            let dir = yield asyncReadDir('rules');
            let promisesRules = [];
            let arrayUrlMethod = [];
            for (let file of dir) {
                let ruleFile = asyncReadFile('rules/' + file, 'utf8');
                promisesRules.push(ruleFile);
            }
            let result = yield Promise.all(promisesRules);
            result.forEach(element => {
                let rule = JSON.parse(element);
                rule = jsonQ(rule);
                let url = rule.find('url').value().at(0);
                let method = rule.find('mainMethod').value().at(0);
                arrayUrlMethod.push(new urlMethod_1.UrlMethod(url, method));
            });
            let or = new OrchestratorRoutes();
            or.generateRoutes(app, arrayUrlMethod);
        });
    }
    generateRoutes(app, arrayUrlMethod) {
        return __awaiter(this, void 0, void 0, function* () {
            //let orchestratorController: OrchestratorController = new OrchestratorController();
            arrayUrlMethod.forEach(urlMethod => {
                switch (urlMethod.mainMethod) {
                    case "GET":
                        app.route(urlMethod.url).get(this.orchestratorController.handle);
                        break;
                    case "POST":
                        app.route(urlMethod.url).post(this.orchestratorController.handle);
                        break;
                    case "PUT":
                        app.route(urlMethod.url).put(this.orchestratorController.handle);
                        break;
                    case "DELETE":
                        app.route(urlMethod.url).delete(this.orchestratorController.handle);
                        break;
                }
            });
        });
    }
}
exports.OrchestratorRoutes = OrchestratorRoutes;
