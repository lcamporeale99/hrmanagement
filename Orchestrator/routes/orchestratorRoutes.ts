import {Application, Request, Response} from "express";
import { OrchestratorController } from "../controllers/orchestratorController";
import { Operation } from '../models/operation';
import { OperationRequest } from '../models/request';
import * as endpoints from '../endpointsMapping'
import { UrlMethod } from '../models/urlMethod';
const jsonQ = require('jsonq');
import request from 'request-promise';
import util from 'util';
import fs from 'fs';

const asyncReadFile = util.promisify(fs.readFile);
const asyncReadDir = util.promisify(fs.readdir);

export class OrchestratorRoutes {

    public orchestratorController: OrchestratorController = new OrchestratorController();

    public async loadRules(app: Application) {

        let dir: string [] = await asyncReadDir('rules'); 
        let promisesRules: Promise<string> []=[];
        
        let arrayUrlMethod: UrlMethod[]=[];

        for (let file of dir){
            let ruleFile = asyncReadFile('rules/' + file, 'utf8');
            promisesRules.push(ruleFile);
            
        }
        let result = await Promise.all(promisesRules);
            result.forEach(element=>{
            let rule = JSON.parse(element);
            rule  = jsonQ(rule);
            let url = rule.find('url').value().at(0);
            let method = rule.find('mainMethod').value().at(0);
            arrayUrlMethod.push(new UrlMethod(url,method))
            
            }
          )
        
        
     let or: OrchestratorRoutes= new OrchestratorRoutes();
      or.generateRoutes(app, arrayUrlMethod); 
    }

    public async generateRoutes(app: Application, arrayUrlMethod: UrlMethod[]){

        //let orchestratorController: OrchestratorController = new OrchestratorController();

        arrayUrlMethod.forEach(urlMethod=>{
            
            
            switch(urlMethod.mainMethod) {
                case "GET":
                    app.route(urlMethod.url).get(this.orchestratorController.handle)
                    break;
                case "POST":
                    app.route(urlMethod.url).post(this.orchestratorController.handle)
                    break;
                case "PUT":
                    app.route(urlMethod.url).put(this.orchestratorController.handle)
                    break;
                case "DELETE":
                    app.route(urlMethod.url).delete(this.orchestratorController.handle)
                    break;
            } 
        })
    }
}