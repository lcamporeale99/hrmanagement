import express from 'express';
import { OrchestratorRoutes } from "./routes/orchestratorRoutes";

class App {

    public app: express.Application;
    public orchestratorRoutes: OrchestratorRoutes = new OrchestratorRoutes();

    constructor() {
        this.app = express();
        this.config();
        //this.orchestratorRoutes.routes(this.app);
    }

    private config(): void {
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
    }
}

export default new App().app;