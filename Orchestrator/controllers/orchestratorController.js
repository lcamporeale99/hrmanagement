"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrchestratorController = void 0;
const operation_1 = require("../models/operation");
const endpoints = __importStar(require("../endpointsMapping"));
const jsonQ = require('jsonq');
const util_1 = __importDefault(require("util"));
const fs_1 = __importDefault(require("fs"));
const asyncReadFile = util_1.default.promisify(fs_1.default.readFile);
const asyncReadDir = util_1.default.promisify(fs_1.default.readdir);
let urls = [];
class OrchestratorController {
    handle() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('ok');
        });
    }
    anagraficaUtenteview(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let rule = yield getRule(req.params.path);
            let operations = yield getOperations(rule);
            let pathParams = yield getPathParams(req);
            let queryParams = yield getQueryParams(req);
            let body = yield getBody(req);
            let headers = yield getHeaders(req);
            console.log(endpoints);
            let data = [{ pathParams, queryParams, body, headers }, { "tenants": [] }];
            operations.forEach(operation => {
                operation.request.pathVariables.forEach((pathVariable) => {
                    retrieveInfo(pathVariable.value, data);
                });
                operation.request.queryParams.forEach((queryParam) => {
                    retrieveInfo(queryParam.value, data);
                });
                operation.request.header.forEach((headerVariable) => {
                    retrieveInfo(headerVariable.value, data);
                });
                let operationBody = JSON.stringify(operation.request.body);
                retrieveInfo(operationBody, data);
                //composeRequest()
            });
        });
    }
}
exports.OrchestratorController = OrchestratorController;
function getRule(serviceName) {
    return __awaiter(this, void 0, void 0, function* () {
        let dir = yield asyncReadDir('rules');
        let file = dir.find((file) => file == serviceName + '.cson');
        let ruleFile = yield asyncReadFile('rules/' + file, 'utf8');
        return JSON.parse(ruleFile);
    });
}
function getOperations(rule) {
    return __awaiter(this, void 0, void 0, function* () {
        let operations = [];
        rule.businessNeeded.forEach(((element) => {
            operations.push(new operation_1.Operation(element.bs, element.endpoint, element.method, element.request, element.order, element.affect));
        }));
        return operations;
    });
}
function getBody(req) {
    return __awaiter(this, void 0, void 0, function* () {
        return req.body;
    });
}
function getPathParams(req) {
    return __awaiter(this, void 0, void 0, function* () {
        return req.params;
    });
}
function getQueryParams(req) {
    return __awaiter(this, void 0, void 0, function* () {
        return req.query;
    });
}
function getHeaders(req) {
    return __awaiter(this, void 0, void 0, function* () {
        return req.headers;
    });
}
function retrieveInfo(param, data) {
    return __awaiter(this, void 0, void 0, function* () {
        if (param != null) {
            if (param.includes('$')) {
                let symbols = param.split('.');
                let index = Number(symbols.at(1));
                let key = symbols.at(2);
                let requestData = jsonQ(data.at(index));
                let value = requestData.find(key).value();
                console.log(key, value);
            }
        }
    });
}
