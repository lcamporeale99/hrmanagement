import { Request, Response } from 'express';
import { Operation } from '../models/operation';
import { OperationRequest } from '../models/request';
import * as endpoints from '../endpointsMapping'
import { UrlMethod } from '../models/urlMethod';
const jsonQ = require('jsonq');
import request from 'request-promise';
import util from 'util';
import fs from 'fs';
import { url } from 'inspector';

const asyncReadFile = util.promisify(fs.readFile);
const asyncReadDir = util.promisify(fs.readdir);

let urls : string [] = [];

class OrchestratorController {

    public async handle(){
        console.log('ok')
    }  
    
    public async anagraficaUtenteview(req: Request, res: Response) {
        let rule = await getRule(req.params.path)
        let operations = await getOperations(rule)
        let pathParams = await getPathParams(req)
        let queryParams = await getQueryParams(req)
        let body = await getBody(req)
        let headers = await getHeaders(req)
        console.log(endpoints)
        let data = [{pathParams, queryParams, body, headers}, {"tenants": []}]

        operations.forEach(operation => {

            operation.request.pathVariables.forEach((pathVariable) => {
                retrieveInfo(pathVariable.value, data)
            })

            operation.request.queryParams.forEach((queryParam) => {
                retrieveInfo(queryParam.value, data)
            })

            operation.request.header.forEach((headerVariable) => {
                retrieveInfo(headerVariable.value, data)
            })

            let operationBody = JSON.stringify(operation.request.body)
            retrieveInfo(operationBody, data)

            //composeRequest()
        })
    }
}

async function getRule(serviceName: string) {
    let dir = await asyncReadDir('rules');
    let file = dir.find((file: string) => file == serviceName + '.cson')
    let ruleFile = await asyncReadFile('rules/' + file, 'utf8');
    return JSON.parse(ruleFile)
}

async function getOperations(rule: any) {
    let operations: Operation[] = []
    rule.businessNeeded.forEach(((element: { bs: string; endpoint: string; method: string; request: OperationRequest; order: number; affect: [Operation]; }) => {
        operations.push(new Operation(element.bs, element.endpoint, element.method, element.request, element.order, element.affect));
    }));
    return operations
}

async function getBody(req: any) {
    return req.body
}

async function getPathParams(req: any) {
    return req.params
}

async function getQueryParams(req: any) {
    return req.query
}

async function getHeaders(req: any) {
    return req.headers
}

async function retrieveInfo(param: string, data: any[]) {
    if (param != null) {
        if (param.includes('$')) {
            let symbols = param.split('.')
            let index = Number(symbols.at(1))
            let key = symbols.at(2)
            
            let requestData = jsonQ(data.at(index))
            let value = requestData.find(key).value();
            console.log(key, value)
        }  
    }  
    
}

// async function composeRequest(params:type) {
//     const options = {
//         uri: `http://localhost:3001/users`,
//         json: true,
//         resolveWithFullResponse: true,
//         method: 'GET'
//     };

//     let response = await request(options);
    
//     res.json(response.body);
// }

export { OrchestratorController }