const {ReadFile} = require('./readFile')
import peggy from 'peggy';
const util = require('util');
const fs = require('fs');
const asyncReadFile = util.promisify(fs.readFile); 

class Variable {
    index: number;
    variableName: string;

    constructor(index: number, variableName: string) {
        this.index = index;
        this.variableName = variableName;
    }
}

class PathVariable {
    name!: string;
    value!: string;
}

class Operation {
    bs: string;
    endpoint: string;
    method: string;
    request: Request;
    order: number;
    affect: [Operation]

    constructor(bs: string, endpoint: string, method: string, request: Request, order: number, affect: [Operation]) {
        this.bs = bs;
        this.endpoint = endpoint;
        this.method = method;
        this.request = request;
        this.order = order;
        this.affect = affect;
    }
}

class Request {
    pathVariables: [PathVariable] | undefined;
    queryParams: [] | undefined;
    body: [] | undefined;
    header: [] | undefined;   
}

export enum RequestTypes {
    "GET" = "get",
    "POST" = "post",
    "PUT" = "put",
    "DELETE" = "delete"
}

export class Parser {
    ruleName: string;

    constructor(ruleName: string) {
        this.ruleName = ruleName
    }

    async generateRequest(this: any, requestType: RequestTypes, pathVariables: []) {
        let response = ""
        let ruleFile = await asyncReadFile('rule.cson', 'utf8');
        console.log(ruleFile)
        let data = JSON.parse(ruleFile)
        response = data
        /*let operations = [Operation]
        data.businessNeeded.forEach((element: typeof Operation) => {
            let operation = new Operation
            operations.push(element);
        });
        operations.at(0)*/
        return response
    }
}



/*let data = JSON.parse(ruleFile)

let bs = data.businessNeeded

let firstRequest = bs.at(0).request
let secondRequest = bs.at(1).request

let variableData = firstRequest.pathVariables.at(0).value.split(".")

let firstRequestVariables: any[] = []

if (variableData.at(0) === '$') {
    firstRequest.pathVariables.forEach((element: any) => {
        firstRequestVariables.push(element);
    });
}

console.log(firstRequestVariables)
}

/*let grammarFile = fs.readFileSync("json.pegjs", "utf-8");

let parser = peggy.generate(grammarFile);

let ruleFile = fs.readFileSync("rule.cson", "utf-8")

let data = parser.parse(ruleFile);*/

/*let variableJson : string
variableJson = data.pathVariables

let variableData = variableJson.split(".");

if (variableData.at(0) === '$') {
    let variable = new Variable(Number(variableData.at(1)), String(variableData.at(2)))
    console.log(variable)
}*/